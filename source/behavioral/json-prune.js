/*!
 * This file is part of eyeo's Anti-Circumvention Snippets module (@eyeo/snippets),
 * Copyright (C) 2006-present eyeo GmbH
 * 
 * @eyeo/snippets is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * @eyeo/snippets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/snippets.  If not, see <http://www.gnu.org/licenses/>.
 */

import $ from "../$.js";
import {apply, proxy} from "proxy-pants/function";
import {hasOwnProperty} from "proxy-pants/object";

import {findOwner} from "../utils/execution.js";
import {getDebugger} from "../introspection/log.js";
import {profile} from "../introspection/profile.js";
import {formatArguments, randomId, toRegExp} from "../utils/general.js";

let {Array, Error, JSON, Map, Object, Response, URL} = $(window);

// will be a Map of all paths, once the snippet is used at least once
let paths = null;

/**
 * Traps calls to JSON.parse, and if the result of the parsing is an Object, it
 * will remove specified properties from the result before returning to the
 * caller.
 *
 * The idea originates from
 * [uBlock Origin](https://github.com/gorhill/uBlock/commit/2fd86a66).
 * @alias module:content/snippets.json-prune
 *
 * @param {string} rawPrunePaths A list of space-separated properties to remove.
 *  Can include placeholders {} and [] to iterate over nested objects and arrays
 *  respectively.
 * @param {?string} [rawNeedlePaths] A list of space-separated properties which
 *   must be all present for the pruning to occur.
 * @param {?string} [rawNeedleStack] A list of space-separated strings or regex
 *   which must be present in the callstack for the pruning to occur.
 *
 * @since Adblock Plus 3.9.0
 */
export function jsonPrune(rawPrunePaths,
                          rawNeedlePaths = "",
                          rawNeedleStack = "") {
  if (!rawPrunePaths)
    throw new Error("Missing paths to prune");

  if (!paths) {
    let debugLog = getDebugger("json-prune");
    const {mark, end} = profile("json-prune");
    mark();

    function pruneObject(obj) {
      for (let {prune, needle, stackNeedle, formattedArgs} of paths.values()) {
        // Check if needle paths are present
        if ($(needle).length > 0 &&
          $(needle).some(path => !findOwner(obj, path)))
          return obj;

        // Check if the call stack matches the rawNeedleStack condition
        if ($(stackNeedle) &&
          $(stackNeedle).length > 0 && !matchesStackTrace(stackNeedle))
          return obj;

        for (let path of prune) {
          if (path.includes("{}") || path.includes("[]"))
            prunePathWithPlaceholders(obj, path, formattedArgs);
          else
            prunePathSimple(obj, path, formattedArgs);
        }
      }
      return obj;
    }

    function prunePathWithPlaceholders(obj, path, formattedArgs) {
      let pathParts = $(path).split(".");
      let currentObj = obj;

      for (let i = 0; i < pathParts.length; i++) {
        let part = pathParts[i];

        if (part === "[]") {
          if (Array.isArray(currentObj)) {
            debugLog("info", `Iterating over array at: ${part}`);
            $(currentObj).forEach(item =>
              prunePathWithPlaceholders(item,
                                        pathParts.slice(i + 1).join("."),
                                        formattedArgs));
          }
          return;
        }
        else if (part === "{}") {
          if (typeof currentObj === "object" && currentObj !== null) {
            debugLog("info", `Iterating over object at: ${part}`);
            Object.keys(currentObj).forEach(key =>
              prunePathWithPlaceholders(currentObj[key],
                                        pathParts.slice(i + 1).join("."),
                                        formattedArgs));
          }
          return;
        }
        else if (currentObj && typeof currentObj === "object" &&
          hasOwnProperty(currentObj, part)) {
          if (i === pathParts.length - 1) {
            debugLog("success", `Found ${path} and deleted, \nFILTER: json-prune ${formattedArgs}`);
            delete currentObj[part];
          }
          else {
            currentObj = currentObj[part];
          }
        }
        else {
          return;
        }
      }
    }

    function prunePathSimple(obj, path, formattedArgs) {
      let details = findOwner(obj, path);
      if (typeof details != "undefined") {
        debugLog("success", `Found ${path} and deleted`, `\nFILTER: json-prune ${formattedArgs}`);
        delete details[0][details[1]];
      }
    }

    /**
     * Checks if the current stack trace matches a given array of strings.
     * It captures the current stack trace by creating a new Error
     * and normalizes it by:
     * - Removing the "at" prefix from each line.
     * - Extracting the function name, URL, and line number from each stack
     * trace line.
     * - Replacing known patterns like inline or anonymous scripts with a
     * readable label.
     * - Ignoring lines that do not contain a resource name or line position.
     * - Prepending a `stackDepth` label that indicates the number of meaningful
     * lines in the stack.
     *
     * The stack trace is transformed into a single string where each relevant
     * line is in a new line (`\n`), in the following format:
     *   stackDepth:<number of lines> <functionName> <url>:<lineNumber>:1
     *
     * Each string in the `stackNeedle` array is converted to a regex and tested
     * against the normalized stack trace.
     *
     * @param {Array<string>} stackNeedle An array of strings to be converted
     *  to regex and checked in the normalized stack trace.
     * @returns {boolean} True if any of the `stackNeedle` patterns match the
     *  normalized stack trace, otherwise false.
     */
    function matchesStackTrace(stackNeedle) {
      if (!stackNeedle)
        return false;

      const token = randomId();
      const error = new Error(token);

      const locHref = new URL(self.location.href);
      locHref.hash = "";

      const lineRegex = /(.*?@)?(\S+)(:\d+):\d+\)?$/;
      const lines = [];
      for (let line of error.stack.split(/[\n\r]+/)) {
        if ($(line).includes(token))
          continue;

        line = $(line).trim();
        const match = $(lineRegex).exec(line);
        if (match === null)
          continue;

        let url = match[2];
        if ($(url).startsWith("("))
          url = $(url).slice(1);

        if (url === locHref.href)
          url = "inlineScript";
        else if ($(url).startsWith("<anonymous>"))
          url = "injectedScript";

        let functionName = match[1] ?
          $(match[1]).slice(0, -1) :
          $(line).slice(0, $(match).index).trim();

        if ($(functionName).startsWith("at"))
          functionName = $(functionName).slice(2).trim();

        let linePosition = match[3];
        $(lines).push(" " + `${functionName} ${url}${linePosition}:1`.trim());
      }

      lines[0] = `stackDepth:${lines.length - 1}`;
      const normalizedStack = $(lines).join("\n");

      for (let needle of stackNeedle) {
        const regex = toRegExp(needle);
        if (regex.test(normalizedStack)) {
          debugLog("info", `Found needle in stack trace: ${needle}`);
          return true;
        }
        debugLog("info", `Needle ${needle} not found in stack trace: ${normalizedStack}`);
      }
    }

    // allow both jsonPrune and jsonOverride to work together
    let {parse} = JSON;
    paths = new Map();

    Object.defineProperty(window.JSON, "parse", {
      value: proxy(parse, function() {
        let result = apply(parse, this, arguments);
        return pruneObject(result);
      })
    });
    debugLog("info", "Wrapped JSON.parse for prune");

    let {json} = Response.prototype;
    Object.defineProperty(window.Response.prototype, "json", {
      value: proxy(json, function() {
        let resultPromise = apply(json, this, arguments);
        return resultPromise.then(obj => pruneObject(obj));
      })
    });
    debugLog("info", "Wrapped Response.json for prune");
    end();
  }

  const formattedArgs = formatArguments(arguments);
  // allow a single unique rawPrunePaths definition per domain
  // TBD: should we throw an error if it was already set?
  paths.set(rawPrunePaths, {
    formattedArgs,
    prune: $(rawPrunePaths).split(/ +/),
    needle: rawNeedlePaths.length ? $(rawNeedlePaths).split(/ +/) : [],
    stackNeedle: rawNeedleStack.length ? $(rawNeedleStack).split(/ +/) : []
  });
}
