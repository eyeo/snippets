/*!
 * This file is part of eyeo's Anti-Circumvention Snippets module (@eyeo/snippets),
 * Copyright (C) 2006-present eyeo GmbH
 * 
 * @eyeo/snippets is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * @eyeo/snippets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/snippets.  If not, see <http://www.gnu.org/licenses/>.
 */

import $ from "../$.js";
import {apply, proxy} from "proxy-pants/function";

import {getDebugger} from "../introspection/log.js";
import {formatArguments, toRegExp} from "../utils/general.js";
import {profile} from "../introspection/profile.js";

const {Error, Object, Array, Map} = $(window);

// Contains all the values to override after the snippet is used at least once
let arrayValues = null;

/**
 * Traps calls to Array.prototype functions. If the needle matches
 * the parameter to the function call, the snippet changes the behaviour
 * of the function to ignore that call or return another value.
 * @alias module:content/snippets.array-override
 *
 * @param {string} method The Array function to override.
 * Possible values to override the property with:
 *   push
 *   includes
 * @param {string} needle The string or regex used to determine
 * which function calls to trap.
 * @param {?string} [returnValue=false] The return value for the matched
 * function calls.
 * Possible values:
 *   false
 *   true
 *
 */
export function arrayOverride(method, needle, returnValue = "false") {
  if (!method)
    throw new Error("[array-override snippet]: Missing method to override.");

  if (!needle)
    throw new Error("[array-override snippet]: Missing needle.");

  if (!arrayValues)
    arrayValues = new Map();

  let debugLog = getDebugger("array-override");
  const {mark, end} = profile("array-override");
  const formattedArgsToLog = formatArguments(arguments);
  // Array.prototype.push
  if (method === "push" && !arrayValues.has("push")) {
    mark();
    const {push} = Array.prototype;
    arrayValues.set("push", $([]));

    Object.defineProperty(window.Array.prototype, "push", {
      value: proxy(push, function(val) {
        // Only strings and numbers are supported for now
        if (!(typeof val === "string" || typeof val === "number"))
          return apply(push, this, arguments);

        const valStr = val.toString();
        const overrideVals = arrayValues.get("push");
        for (const {needleRegex} of overrideVals) {
          if (valStr.match && valStr.match(needleRegex)) {
            debugLog("success", `Array.push is ignored for needle: ${needleRegex}\nFILTER: array-override ${formattedArgsToLog}`);
            return;
          }
        }
        return apply(push, this, arguments);
      })
    });
    debugLog("info", "Wrapped Array.prototype.push");
    end();
  }
  // Array.prototype.includes
  else if (method === "includes" && !arrayValues.has("includes")) {
    mark();
    const {includes} = Array.prototype;
    arrayValues.set("includes", $([]));

    Object.defineProperty(window.Array.prototype, "includes", {
      value: proxy(includes, function(val) {
        // Only strings and numbers are supported for now
        if (!(typeof val === "string" || typeof val === "number"))
          return apply(includes, this, arguments);

        const valStr = val.toString();
        const overrideVals = arrayValues.get("includes");
        for (const {needleRegex, retVal} of overrideVals) {
          if (valStr.match && valStr.match(needleRegex)) {
            debugLog("success", `Array.includes returned ${retVal} for ${needleRegex}\nFILTER: array-override ${formattedArgsToLog}`);
            return retVal;
          }
        }
        return apply(includes, this, arguments);
      })
    });
    debugLog("info", "Wrapped Array.prototype.includes");
    end();
  }

  const needleRegex = toRegExp(needle);
  const overrideVals = arrayValues.get(method);
  const retVal = returnValue === "true";
  overrideVals.push({needleRegex, retVal});
  arrayValues.set(method, overrideVals);
}
